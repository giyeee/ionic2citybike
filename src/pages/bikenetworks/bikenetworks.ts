//angular y ionic
import { Component,Pipe } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
//providers
import { CitybikeBikenetworks } from '../../providers/citybike-bikenetworks';
//models
import { Network } from '../../models/network';
import { NetworkdetailPage } from '../networkdetail/networkdetail'

@Component({
  selector: 'page-bikenetworks',
  templateUrl: 'bikenetworks.html'
})
export class BikenetworksPage {
  networks: Network[];
  allnetworks: Network[];
  searching: any = true;


  constructor(public navCtrl: NavController, private citybikebikenetworks: CitybikeBikenetworks) {
     citybikebikenetworks.load().subscribe(networks => {
       this.networks=networks.sort((a, b) => {
        var o1 = a.location.country;
        var o2 = b.location.country;
        var p1 = a.location.city;
        var p2 = b.location.city;
        if (o1 < o2) return -1;
        if (o1 > o2) return 1;
        if (p1 < p2) return -1;
        if (p1 > p2) return 1;
        return 0;
      });

       this.allnetworks = this.networks;
       this.searching = false;

     })
   }
   goToDetails(href: string,name: string,city: string,latitude: number,longitude: number) {
    this.navCtrl.push(NetworkdetailPage, {href,name,city,latitude,longitude});
  }
  initializeItems() {
    this.networks = this.allnetworks;
  }

  getItems(ev: any) {
    this.searching = true;
    this.initializeItems();
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.networks = this.networks.filter((item) => {
        return (item.location.city.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    this.searching = false;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BikenetworksPage');
  }
  
  focusOut() {
  let activeElement = <HTMLElement>document.activeElement;
  activeElement && activeElement.blur && activeElement.blur();
}
}


//
// @Pipe({name: 'sortArrayOfCustomObjects'})
// export class SortArrayOfCustomObjectsPipe implements PipeTransform {
//
//   transform(arr: Network[], args: any): Network[]{
//     if(!arr){ return; }
//     return arr.sort((a, b) => {
//       if (a.location.city > b.location.city || a.location.city < b.location.city) {
//         return 1;
//       }
//       if (a.location.city < b.location.city || a.location.city > b.location.city) {
//         return -1;
//       }
//       return 0;
//     });
//   }
// }
