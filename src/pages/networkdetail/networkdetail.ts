import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CitybikeBikenetworks } from '../../providers/citybike-bikenetworks';
import { BikeStation } from '../../models/bikestation';

declare var google;

@Component({
  selector: 'page-networkdetail',
  templateUrl: 'networkdetail.html'
})
export class NetworkdetailPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  name: string;
  city: string;
  networkurl: string;
  latitude: number;
  longitude: number;
  searching: any = true;

  bikestations: BikeStation[]
  constructor(public navCtrl: NavController, private navParams: NavParams, private citybikebikenetworks: CitybikeBikenetworks) {
    this.name = navParams.get('name');
    this.city = navParams.get('city');
    this.latitude = navParams.get("latitude");
    this.longitude = navParams.get("longitude");
    this.networkurl = navParams.get('href');

    citybikebikenetworks.loadDetail(this.networkurl).subscribe(bikestations => {
      this.bikestations = bikestations;
      this.searching = false;
      setTimeout(this.loadBikeStations(),5000);
    })

  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad NetworkdetailPage');
  }

  ngOnInit(){
    this.loadMap();

  }
  loadMap(){
    let latLng = new google.maps.LatLng(this.latitude, this.longitude);
    let mapOptions = {
      center: latLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  }

  loadBikeStations(){
    // Add the markerto the map
    for (var i = 0; i < this.bikestations.length; i++) {

         var bikestation = this.bikestations[i];
         var markerPos = new google.maps.LatLng(bikestation.latitude, bikestation.longitude);

         // Add the markerto the map
         if(bikestation.free_bikes==0){
           var marker = new google.maps.Marker({
               map: this.map,
               icon: '../assets/images/cycling-occupied.png',
               animation: google.maps.Animation.DROP,
               position: markerPos
           });
         }else if(bikestation.free_bikes<4){
           var marker = new google.maps.Marker({
               map: this.map,
               icon: '../assets/images/cycling-fewavailable.png',
               animation: google.maps.Animation.DROP,
               position: markerPos
           });
         } else {
           var marker = new google.maps.Marker({
               map: this.map,
               icon: '../assets/images/cycling-available.png',
               animation: google.maps.Animation.DROP,
               position: markerPos
           });
         }

         var infoWindowContent = "<h4>" + bikestation.name + "</h4> <span>Bicis para usar: " +bikestation.free_bikes+" </span></br> <span>Lugares vacios: " + bikestation.empty_slots + "</span>";

         this.addInfoWindow(marker, infoWindowContent, bikestation);

       }
  }

  addInfoWindow(marker, message, bikestation) {

      var infoWindow = new google.maps.InfoWindow({
          content: message
      });

      google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open(this.map, marker);
      });

  }

}
