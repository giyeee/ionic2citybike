export interface BikeStation {
  empty_slots: number;
  free_bikes: number;
  latitude: number;
  longitude: number;
  name: string;
}
