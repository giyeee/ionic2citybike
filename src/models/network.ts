
import { NetLocation } from './netlocation';
export interface Network {
  href: string;
  id: number;
  location: NetLocation;
  name: string
}
