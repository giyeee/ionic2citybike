export interface NetLocation {
  city: string;
  country: string;
  latitude: number;
  longitude: number;
}
