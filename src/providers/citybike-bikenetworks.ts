import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Network } from '../models/network';
import { BikeStation } from '../models/bikestation';

@Injectable()
export class CitybikeBikenetworks {
  cityBikeUrl = 'https://api.citybik.es';

  constructor(public http: Http) { }

  load(): Observable<Network[]> {
    return this.http.get(`${this.cityBikeUrl}/v2/networks`)
      .map(res => res.json().networks);
  }
  loadDetail(url: string): Observable<BikeStation[]> {
    return this.http.get(`${this.cityBikeUrl + url}`)
      .map(res => res.json().network.stations);
  }
}
